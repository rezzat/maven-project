import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {
    private static Logger log;
    private static ResourceBundle resourceBundle;

    public static void main(String[]args)
    {
        log = Logger.getLogger(Main.class.getName());
        
        resourceBundle = ResourceBundle.getBundle("Profile");
        
        log.setLevel(Level.ALL);

        String profileName = resourceBundle.getString("nameofProfileResource");

        if(profileName.equals("FirstProfile"))
            log.info("Hello World printed from Profile: Profile 1");
        else {
        	if (profileName.equals("SecondProfile"))
            log.info("Hello World printed from Profile: Profile 2");
        }
    }
}